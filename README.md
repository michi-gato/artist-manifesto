# Statement From Sarah Nguyễn

This repo is a single webpage ([https://michi-gato.gitlab.io/artist-statement/](https://michi-gato.gitlab.io/artist-statement/)) as a reflection of creation and process as an artist. The webpage is a digital collage that consists manifestos, statements, lists inspired by other creators who have inspired me.

# Prompt
This page began as an assignment for a Dance Composition course, a in the Dance MFA program. The assignment, titled "Artist Manifesto", is the only written-based assignment produced from this course.

# My Manifesto
"All of the [social justice] elements presented here are of import; they build upon each other sequentially, and all should be addressed throughout the year." (<a href="https://ijme-journal.org/ijme/index.php/ijme/article/view/484">Picower, 2012</a>)

I designed a creative-making experience where I could get into "the zone" (see [flow state](https://en.wikipedia.org/wiki/Flow_(psychology)) and [Soul, the animated film](https://www.cbr.com/soul-zone-lost-souls-two-sides-same-coin/)), which makes space for me to recognize and express my physical and mental needs and capacities. I do this through three phases: a morning walk through a forestry creek park, capture movement and dance in reaction to the sounds of the forest and obstacles in the natural environment, and then create a digital record of the experience to share with my family and community members. I explore the three paradigms of Positive Design framework throughout each phase and will continue to reflective on how I touch upon virtue, pleasure, and personal significance within and after each phase. I will use this unbound time to nurture movement-based practice into my daily routine. This will situate me in this new environment that I recently moved into, and help me express the threads of intergenerational narratives and face social injustices that I am actively incorporating throughout my individual and collaborative life works.

# Sources

Picower, B. (2012). Using Their Words: Six Elements of Social Justice Curriculum Design for the Elementary Classroom. International Journal of Multicultural Education, 14(1). https://eric.ed.gov/?id=EJ1105049
